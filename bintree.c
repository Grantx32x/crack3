#include <stdlib.h>
#include <stdio.h>
#include "bintree.h"

void insert(char *pass, char *hash, node **leaf)
{
    char *key = *hash;
    char *s = md5(pass, strlen(pass));
    if (*leaf == NULL)
    {
        *leaf = (node *) malloc(sizeof(node));
        strcpy(s, hash);
        (*leaf)->data = key;
        /* initialize the children to null */
        (*leaf)->left = NULL;    
        (*leaf)->right = NULL;  
    }
    else if (key < (*leaf)->data)
        insert (key, s, &((*leaf)->left) );
    else if (key > (*leaf)->data)
        insert (key, s, &((*leaf)->right) );
    
    free(s);
}

void print(node *leaf)
{
	if (leaf == NULL) return;
	if (leaf->left != NULL) print(leaf->left);
	printf("%d\n", leaf->data);
	if (leaf->right != NULL) print(leaf->right);
}

// TODO: Modify so the key is the hash to search for
node *search(char *hashes, node *leaf)// search for string instead of int key
{
  if (leaf != NULL)
  {
      if (hashes == leaf->hash)
         return leaf;
      else if (hashes < leaf->hash)
         return search(hashes, leaf->left);
      else
         return search(hashes, leaf->right);
  }
  else return NULL;
}

/*
int main()
{
    node *tree = NULL;
    
    insert(5, &tree);
    insert(2, &tree);
    insert(8, &tree);
    insert(10, &tree);
    insert(1, &tree);
    insert(4, &tree);
    insert(8, &tree);
    insert(7, &tree);

    print(tree);
}
*/
