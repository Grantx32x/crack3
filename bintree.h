// TODO: modify the struct so it holds both the plaintext
// word and the hash.
typedef struct node {
	int data; 
	char hash[33];
	char pass[50];
	struct node *left;
	struct node *right;
} node;

void insert(char *pass, char *hash, node **leaf);

void print(node *leaf);

node *search(char *hashes, node *leaf);
